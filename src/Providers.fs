﻿module uTools.Providers

open System.Collections.Generic
open System.IO
open System.Net

let private dictionary = Dictionary<string, string>()

let cache (get: string -> string) key =
    if dictionary.ContainsKey key |> not then
        dictionary.[key] <- get key
    dictionary.[key]

let getProvider =
    function
    | "gitlab" -> fun (lib: string) ->
        use http = new WebClient()
        lib.Replace("\\","/").Split('/')
        |> fun parts ->
            parts |> Seq.skip 2 |> String.concat "/"
            |> sprintf "https://gitlab.com/%s/raw/master/%s" (parts |> Seq.take 2 |> String.concat "/")
            |> cache http.DownloadString
    | "http" | "https" as name -> fun (lib: string) ->
        use http = new WebClient()
        sprintf "%s://%s" name lib
        |> cache http.DownloadString
    | "file" -> fun (lib: string) -> cache File.ReadAllText lib
    | _ -> failwith "Wrong provider"