﻿namespace uTools

open System.IO
open uTools.Helpers

type LangRender =
    {
        renderFile: ToolFile -> string -> string
        renderModule: string -> string
        endModule: string
        renderCommentBlock: bool -> string
        renderComment: string -> string
        isFunHeader: string -> bool
        isImport: string -> bool
        isPrivate: string -> bool
    }
    member rend.isComment (line: string) =
        line.TrimStart().StartsWith(rend.renderComment "")
    static member Parse (file: FileInfo) =
        match file.Extension with
        | ".fs" | ".fsx" -> "F#"
        | ".cs" | ".csx" -> "C#"
        | _ -> file.Extension
        
    static member Create lang =
        match lang with
        | "C#" -> sprintf "LangRender for C# to be done soon. Check %s for updates" repolink |> failwith
        | "F#" ->
            { renderFile = fun file main ->
                zip [
                  "module private " + Path.GetFileNameWithoutExtension file.name
                  "(*"
                  file.config
                  "*)"
                  "//" + disclaimer
                  file.imports
                  main ]
              renderModule = fun name -> "module " + name + " ="
              endModule = ""
              renderCommentBlock = fun close -> if close then "*)" else "(*"
              renderComment = fun cmt -> "//" + cmt
              isFunHeader = fun line -> line.StartsWith "let "
              isImport = fun line -> line.StartsWith "open "
              isPrivate = fun line -> line.Contains " private " }
        | _ -> sprintf "LangRender %s not found" lang |> failwith