﻿namespace uTools

type ToolFun =
    {
        header: string        
        mutable lines: string list
    }
type ToolModule =
    {
        ignoring: string -> string
        level: int
        name: string
        mutable order: int
        mutable modules: ToolModule list
        mutable tools: ToolFun list
    }
type ToolFile =
    {
        name: string
        config: string
        imports: string
        modules: ToolModule array
    }