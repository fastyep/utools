﻿module uTools.FileParser

open System
open System.IO
open uTools
open uTools.Helpers

type private Tool =
    {
        order: int
        modules: string array
        provider: string
        lib: string
        tools: (string * string) array
    }
    member tool.HasFun (render: LangRender) (func: ToolFun) =
        if tool.tools.Length = 0 || render.isPrivate func.header then Some func else
        tool.tools
        |> Seq.tryFind (fun (_, get) -> func.header.Contains get)
        |> Option.map (fun (need, get) ->
            let rep = rep (get, need)
            func.lines <- func.lines |> List.map rep
            { func with header = rep func.header } )

    
let private parseTool (order: int) (mods: string) (part: string) =
    let url = part.Split([|"://"|], StringSplitOptions.RemoveEmptyEntries)
    let uri = url.[1].Split('?', StringSplitOptions.RemoveEmptyEntries)
    let mapper (a: string) = a.Split '=' |> fun a -> (Seq.head a, Seq.last a)
    { order = order
      modules = mods.Split '.'
      provider = url.[0].ToLower()
      lib = uri.[0]
      tools =
        uri
        |> Seq.skip 1
        |> Seq.tryHead
        |> Option.map (fun a -> a.Split '&' |> Seq.map mapper |> Seq.toArray)
        |> Option.defaultValue [||] }    
    
let private parseTools (order: int) (line: string) =
    let parts = line.Split ' '
    let parseTool = parseTool order parts.[0]
    parts
    |> Seq.skip 1
    |> Seq.map parseTool
    
let private parseSourceFile (render: LangRender, import: string -> unit) (tool: Tool) =
    let mutable tools : ToolFun list = []
    Providers.getProvider tool.provider tool.lib    
    |> unzip
    |> Seq.iter(fun line ->
        if line.Trim() = "" then ()
        elif render.isComment line then ()
        elif render.isFunHeader line then tools <- { header = line; lines = [] } :: tools
        elif render.isImport line then import line
        elif tools.Length > 0 then tools.Head.lines <- tools.Head.lines @ [line])
    tools
    |> List.rev
    |> List.choose (tool.HasFun render)
    
let parseFile (render: LangRender) (file: FileInfo) (forced: bool) =
    let mutable stage = 0uy
    let mutable tree : ToolModule list = []
    let mutable imports : string list = []
    let fileLines = 
        Providers.cache File.ReadAllText file.FullName
        |> unzip
    let ignoreLines =
        if forced then [||] else
        fileLines
        |> Array.choose (fun line ->
            let t = line.TrimStart()
            let ch = render.renderComment ""
            if t.StartsWith ch then
                let t = t.Substring(ch.Length).TrimStart()
                Some (t, [line])
            else None)
    let ignoring (line: string) =
        if forced then line else
        ignoreLines
        |> Seq.tryFind (fst >> (line.TrimStart() |> (=)))
        |> Option.map (snd >> List.head)
        |> Option.defaultValue line
    let rec buildTree (now: ToolModule option) (tool: Tool) =
        let level = now |> Option.map (fun a -> a.level) |> Option.defaultValue 0
        let name = tool.modules.[level]
        now
        |> Option.map (fun a -> a.modules)
        |> Option.defaultValue tree
        |> Seq.tryFind (fun a -> a.name = name)
        |> Option.defaultWith (fun () ->
            let created =
                { ignoring = ignoring
                  order = 0
                  level = level + 1
                  name = name
                  modules = []
                  tools = [] }
            match now with
            | Some modn -> modn.modules <- created :: modn.modules
            | None -> tree <- created :: tree
            created)
        |> fun result ->
            let import space = imports <- space :: imports
            if level = tool.modules.Length - 1 then
                if result.order < tool.order then result.order <- tool.order
                result.tools <- result.tools @ parseSourceFile (render, import) tool
            else buildTree (Some result) tool
    let configLines =
        fileLines
        |> Array.choose (fun line ->
            match stage with
            | 0uy ->
                if line.Trim() = render.renderCommentBlock false then stage <- 1uy
                None
            | 1uy ->
                let line = line.Trim()
                if line <> render.renderCommentBlock true then Some line
                else stage <- 2uy; None
            | _ -> None)
    configLines
    |> Seq.mapi parseTools
    |> Seq.concat
    |> Seq.iter (buildTree None)
    { name = file.Name
      config = zip configLines
      modules = Seq.toArray tree
      imports = imports |> Seq.distinct |> Seq.sort |> Seq.map ignoring |> zip }

let private renderTool (modul: ToolModule) (tool: ToolFun) =
    let head = tool.header |> printab modul.level
    let body = tool.lines |> List.map (printab modul.level)
    head :: body
    |> Seq.map modul.ignoring
    |> zip
    
let private moduleSort (modu: ToolModule) = modu.order
    
let rec private renderModule (render: LangRender) (modul: ToolModule) =
    [
        render.renderModule modul.name |> printab (modul.level - 1)
        modul.tools |> Seq.map (renderTool modul) |> zip
        modul.modules |> Seq.sortBy moduleSort |> Seq.map (renderModule render) |> zip
        render.endModule |> printab (modul.level - 1)
    ] |> zip
    
let renderFile render (file: ToolFile) =
    file.modules
    |> Seq.sortBy moduleSort
    |> Seq.map (renderModule render)
    |> zip
    |> render.renderFile file