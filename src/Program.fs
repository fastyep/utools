﻿module uTools.Console

open System
open System.IO
open uTools.FileParser

let private autoTarget dir =
    if Directory.Exists dir then
        DirectoryInfo(dir).EnumerateFiles "uTools.*"
        |> Seq.tryHead
        |> Option.map Ok
        |> Option.defaultWith (fun _ -> failwith "Target file not found, use `-f -t {file}` to create it")
    else
        dir.Replace("*", "uTools")
        |> FileInfo
        |> Error
    
let private isForce =
    Array.exists (fun a -> a = "-f" || a = "--force")

let private getTarget argv =
    argv
    |> Array.tryFindIndex (fun a -> a = "-t" || a = "--target")
    |> Option.map (fun index -> argv.[index + 1])
    |> Option.map (fun file -> if File.Exists file then FileInfo file |> Ok else autoTarget file)
    |> Option.defaultWith (Directory.GetCurrentDirectory >> autoTarget)
    |> function
    | Ok file -> file
    | Error file ->
        if isForce argv then
            File.WriteAllBytes(file.FullName, Array.zeroCreate 0) |> ignore
        else
            sprintf "Target %s doesn't exist, use -f or --force argument to create it" file.FullName |> failwith
        file
    
let private getLang argv =
    argv
    |> Array.tryFindIndex (fun a -> a = "-l" || a = "--language")
    |> Option.map (fun index -> argv.[index + 1])

[<EntryPoint>]
let main argv =
    let file = getTarget argv
    let render = getLang argv |> Option.defaultValue (LangRender.Parse file) |> LangRender.Create
    isForce argv
    |> parseFile render file
    |> renderFile render
    |> fun code -> File.WriteAllText (file.FullName, code)
    0
